package com.ubidev.repository;

import com.ubidev.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * User JPA repository
 */
@Repository
public interface  UserRepository extends JpaRepository<User, Long> {

    User findByEmail(String email);
}
