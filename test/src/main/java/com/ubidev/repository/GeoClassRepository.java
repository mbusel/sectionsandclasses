package com.ubidev.repository;

import com.ubidev.entities.GeoClass;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * GeoClass JPA repository
 */
@Repository
public interface GeoClassRepository extends JpaRepository<GeoClass, Long> {

    List<GeoClass> findByNameContainingAndCodeContaining(String name, String code);

    List<GeoClass> findByNameContaining(String name);

    List<GeoClass> findByCodeContaining(String code);

}
