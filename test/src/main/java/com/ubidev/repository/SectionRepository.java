package com.ubidev.repository;

import com.ubidev.entities.Section;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * Section JPA repository
 */
@Repository
public interface SectionRepository extends JpaRepository<Section, Long> {

    List<Section> findByJobId(Long jobId);

}