package com.ubidev.repository;

import com.ubidev.entities.Job;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * Job JPA repository
 */
@Repository
public interface JobRepository extends JpaRepository<Job, Long> {

    List<Job> findByFilePathIsNotNull();

}