package com.ubidev.exception;

/**
 * Base exception class
 */
public class BaseException extends Exception {

    private static final long serialVersionUID = -8555771702508554355L;

    private String code;

    public String getCode() {
        return this.code;
    }

    public BaseException(String code) {
        this.code = code;
    }

    public BaseException(String code, String message) {
        super(message);
        this.code = code;
    }

    public BaseException(String code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }
}
