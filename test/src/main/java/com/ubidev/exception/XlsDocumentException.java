package com.ubidev.exception;

/**
 * XlS Document exception
 * This exception is used as exception wrapper for all XLS parse/create exceptions
 */
public class XlsDocumentException extends BaseException {

    private static final long serialVersionUID = -5859211168228606938L;

    public XlsDocumentException(String message) {
        super(Error.ERROR_XLS, message);
    }
}