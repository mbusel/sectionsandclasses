package com.ubidev.exception;

/**
 * Errors codes which are used in REST API response objects.
 */
public class Error {

    public final static String ERROR_UNAUTHORIZED = "100";
    public final static String ERROR_ACCESS_DENIED = "101";
    public final static String ERROR_INVALID_CREDENTIALS = "102";
    public final static String ERROR_USER_DISABLED = "103";

    public final static String ERROR_FILE_UPLOAD = "200";

    public final static String ERROR_XLS = "300";

    private Error() {
    }
}