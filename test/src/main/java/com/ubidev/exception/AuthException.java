package com.ubidev.exception;

/**
 * Auth exception
 */
public class AuthException extends BaseException {

    private static final long serialVersionUID = -7667255905039925626L;

    public AuthException(String code, String message) {
        super(code, message);
    }
}
