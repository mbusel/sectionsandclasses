package com.ubidev.exception;

/**
 * FileUpload exception
 */
public class FileUploadException extends BaseException {

    private static final long serialVersionUID = -6767582798798282466L;

    public FileUploadException(String code, String message) {
        super(code, message);
    }
}
