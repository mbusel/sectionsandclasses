package com.ubidev.dto;

import lombok.Getter;

/**
 * Error data transfer object
 */
@Getter
public class ErrorDto extends BaseDto {

    private static final long serialVersionUID = -2627284789436830518L;

    private String error;
    private String message;

    public ErrorDto(String code, String message) {
        this.error = code;
        this.message = message;
    }
}
