package com.ubidev.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Job data transfer object
 */
@Getter @Setter
public class JobDto extends BaseDto {

    private static final long serialVersionUID = 2855506658996120686L;

    private Long id;
    private String error;
    private Boolean ready;

    public Boolean getReady() {
        return this.ready;
    }

    public void setReady(Boolean ready) {
        this.ready = ready;
    }
}