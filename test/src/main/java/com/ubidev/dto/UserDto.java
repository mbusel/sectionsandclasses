package com.ubidev.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * User data transfer object
 */
@Getter @Setter
public class UserDto extends BaseDto {

    private static final long serialVersionUID = -574440293666327756L;

    private Long id;
    private String email;
    private String firstName;
    private String lastName;
}
