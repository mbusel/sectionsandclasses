package com.ubidev.dto;

import java.io.Serializable;

/**
 * Base class for all data transfer objects
 */
public abstract class BaseDto implements Serializable {

    private static final long serialVersionUID = -7077831272471260758L;

}
