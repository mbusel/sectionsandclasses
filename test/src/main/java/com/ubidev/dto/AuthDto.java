package com.ubidev.dto;

import lombok.Getter;
import lombok.Setter;
import java.util.List;

/**
 * Auth data transfer object
 */
@Getter @Setter
public class AuthDto extends BaseDto {

    private static final long serialVersionUID = -8518736414164393250L;

    private String email;
    private String firstName;
    private String lastName;
    private List<String> roles;

    public List<String> getRoles() {
        return this.roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }
}
