package com.ubidev.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * GeoClass data transfer object
 */
@Getter @Setter
public class GeoClassDto extends BaseDto {

    private static final long serialVersionUID = 332851744424544478L;

    private String name;
    private String code;
    private String section;

    public String getSection() {
        return this.section;
    }

    public void setSection(String section) {
        this.section = section;
    }
}
