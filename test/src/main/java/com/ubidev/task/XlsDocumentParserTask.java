package com.ubidev.task;

import com.ubidev.entities.Job;
import com.ubidev.entities.Section;
import com.ubidev.exception.XlsDocumentException;
import com.ubidev.service.JobService;
import com.ubidev.service.SectionService;
import com.ubidev.utils.XlsUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import java.io.File;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * This class represent XML document parser task
 * It parses XLS file and store its data as sections and geo classes
 */
@Component
public class XlsDocumentParserTask {

    protected Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Value("${parse.xls.delay}")
    private Long delay;

    private JobService jobService;
    private SectionService sectionService;
    private XlsUtils xlsUtils;

    public XlsDocumentParserTask(JobService jobService, SectionService sectionService, XlsUtils xlsUtils) {
        this.jobService = jobService;
        this.sectionService = sectionService;
        this.xlsUtils = xlsUtils;
    }

    @PostConstruct
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void scheduleTask() {
        TimerTask task = new TimerTask() {
            public void run() {
                try {
                    List<Job> jobs = jobService.findInProgressJobs();
                    if (jobs != null && !jobs.isEmpty()) {
                        for (Job job : jobs) {

                            File xlsFile = new File(job.getFilePath());
                            try {
                                List<Section> sections = xlsUtils.parseSectionsFromFile(xlsFile);
                                job.setFilePath(null);
                                jobService.update(job);
                                for (Section section : sections) {
                                    section.setJob(job);
                                    sectionService.create(section);
                                }
                            } catch (XlsDocumentException e) {
                                job.setFilePath(null);
                                job.setError(e.getMessage());
                                jobService.update(job);
                            } finally {
                                xlsFile.delete();
                            }
                        }
                    }
                } catch (Exception e) {
                    LOGGER.error("Timer task", e);
                } finally {
                    scheduleTask();
                }
            }
        };

        new Timer().schedule(task, this.delay);
    }
}