package com.ubidev.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * UserRole JPA entity
 */
@Entity
@Table(name = "user_role", uniqueConstraints = @UniqueConstraint(columnNames = {"user_id", "name"}))
public class UserRole implements Serializable {

    private static final long serialVersionUID = -1893868658181741043L;

    public final static String ROLE_ADMIN = "ADMIN";
    public final static String ROLE_USER = "USER";

    @Id
    @GeneratedValue
    private Long id;

    @Column(name="name", nullable=false, length=15)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public UserRole() {
    }

    public UserRole(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "UserRole{" +
                "id=" + this.id +
                ", name='" + this.name + '\'' +
                '}';
    }
}
