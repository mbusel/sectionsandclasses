package com.ubidev.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * GeoClass JPA entity
 */
@Entity
@Table(name = "geological_class")
public class GeoClass implements Serializable {

    private static final long serialVersionUID = -317503162069960573L;

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "name", nullable=false, length=100)
    private String name;

    @Column(name = "code", nullable=false, length=5)
    private String code;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "section_id", nullable = false)
    private Section section;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Section getSection() {
        return this.section;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    public GeoClass() {
    }

    public GeoClass(String name, String code, Section section) {
        this.name = name;
        this.code = code;
        this.section = section;
    }

    @Override
    public String toString() {
        return "GeologicalClass{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
