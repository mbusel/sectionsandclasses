package com.ubidev.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Section JPA entity
 */
@Entity
@Table(name="section")
public class Section implements Serializable {

    private static final long serialVersionUID = -9069757457043632718L;

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "name", nullable=false, length=100)
    private String name;

    @OneToMany(mappedBy = "section", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST})
    private List<GeoClass> geoClasses;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "job_id", nullable = false)
    private Job job;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<GeoClass> getGeoClasses() {
        return this.geoClasses;
    }

    public void setGeoClasses(List<GeoClass> geoClasses) {
        this.geoClasses = geoClasses;
    }

    public Job getJob() {
        return this.job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public Section() {
    }

    public Section(String name, List<GeoClass> geoClasses) {
        this.name = name;
        this.geoClasses = geoClasses;
    }

    @Override
    public String toString() {
        return "Section{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}