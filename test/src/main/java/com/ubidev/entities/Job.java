package com.ubidev.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Job JPA entity
 */
@Entity
@Table(name = "job")
public class Job implements Serializable {

    private static final long serialVersionUID = -4094585015194574609L;

    @Id
    @GeneratedValue
    private Long id;

    @Column(name="file_path", length=255)
    private String filePath;

    @Column(name="error", length=255)
    private String error;

    @OneToMany(mappedBy = "job", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST})
    private List<Section> sections;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFilePath() {
        return this.filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public List<Section> getSections() {
        return this.sections;
    }

    public void setSections(List<Section> sections) {
        this.sections = sections;
    }

    public String getError() {
        return this.error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "Job{" +
                "filePath='" + filePath + '\'' +
                ", error='" + error + '\'' +
                '}';
    }
}