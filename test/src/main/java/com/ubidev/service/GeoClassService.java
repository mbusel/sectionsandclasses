package com.ubidev.service;

import com.ubidev.entities.GeoClass;
import java.util.List;

public interface GeoClassService {

    List<GeoClass> findByNameAndCode(String name, String code);
}
