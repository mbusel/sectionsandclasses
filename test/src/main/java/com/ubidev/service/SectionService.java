package com.ubidev.service;

import com.ubidev.entities.Section;

import java.util.List;

public interface SectionService {

    Section create(Section section);

    Section read(Long sectionId);

    List<Section> findByJobId(Long jobId);
}