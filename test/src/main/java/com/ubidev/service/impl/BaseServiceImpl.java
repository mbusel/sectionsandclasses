package com.ubidev.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.PostConstruct;

/**
 * Base service class
 */
public abstract class BaseServiceImpl {

    protected Logger LOGGER;

    @PostConstruct
    public void initializeLogger() {
        LOGGER = LoggerFactory.getLogger(this.getClass());
    }
}
