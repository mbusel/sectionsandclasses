package com.ubidev.service.impl;

import com.ubidev.entities.User;
import com.ubidev.repository.UserRepository;
import com.ubidev.security.SecurityUtil;
import com.ubidev.service.AuthService;
import org.hibernate.Hibernate;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

/**
 * Auth service
 */
@Service
public class AuthServiceImpl extends BaseServiceImpl implements AuthService {

    private UserRepository userRepository;
    private SecurityUtil securityUtil;

    /**
     * Auth service constructor
     * @param userRepository user repository
     * @param securityUtil security utils
     */
    public AuthServiceImpl(UserRepository userRepository, SecurityUtil securityUtil) {
        this.userRepository = userRepository;
        this.securityUtil = securityUtil;
    }

    /**
     * Find user with roles by email
     * @param email email
     * @return user
     */
    @Override
    public User findUserWithRoles(String email) {
        return findByEmail(email);
    }

    /**
     * Find user by email
     * @param email email
     * @return user
     */
    @Transactional(Transactional.TxType.NEVER)
    public User findByEmail(String email) {
        User user = this.userRepository.findByEmail(email);
        if (user != null) {
            Hibernate.initialize(user.getRoles());
        }
        return user;
    }

    /**
     * Load user details by email
     * @param email email
     * @return user details
     */
    @Override
    @Transactional(Transactional.TxType.NEVER)
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = findUserWithRoles(email);
        if (user == null) {
            throw new UsernameNotFoundException("Invalid email");
        }
        return this.securityUtil.toUserDetails(user);
    }
}
