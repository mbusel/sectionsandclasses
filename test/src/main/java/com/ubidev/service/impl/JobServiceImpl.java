package com.ubidev.service.impl;

import com.ubidev.entities.Job;
import com.ubidev.exception.Error;
import com.ubidev.exception.FileUploadException;
import com.ubidev.repository.JobRepository;
import com.ubidev.service.JobService;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import javax.transaction.Transactional;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.List;

/**
 * Job service
 */
@Service
@Transactional(Transactional.TxType.REQUIRED)
public class JobServiceImpl extends BaseServiceImpl implements JobService {

    private JobRepository jobRepository;

    /**
     * Job service constructor
     * @param jobRepository job repository
     */
    public JobServiceImpl(JobRepository jobRepository) {
        this.jobRepository = jobRepository;
    }

    /**
     * Create job from uploaded XLS file
     * @param file XLS file
     * @return job
     */
    @Override
    public Job create(MultipartFile file) throws FileUploadException {

        Job job;

        try {

            Path tempFilePath = Files.createTempFile("doc", ".xls");
            InputStream stream = file.getInputStream();
            Files.copy(stream, tempFilePath, StandardCopyOption.REPLACE_EXISTING);

            job = new Job();
            job.setFilePath(tempFilePath.toAbsolutePath().toString());

        } catch (IOException e) {
            throw new FileUploadException(Error.ERROR_FILE_UPLOAD, e.getMessage());
        }

        return this.jobRepository.save(job);
    }

    /**
     * Read job by ID
     * @param jobId job ID
     * @return job
     */
    @Override
    public Job read(Long jobId) {
        return this.jobRepository.getOne(jobId);
    }

    /**
     * Read all jobs
     * @return jobs
     */
    @Override
    public List<Job> readAll() {
        return this.jobRepository.findAll(Sort.by(Sort.Order.asc("id")));
    }

    /**
     * Update job
     * @param job job
     */
    @Override
    public void update(Job job) {
        this.jobRepository.save(job);
    }

    /**
     * Find only In Progress jobs
     * @return In Progress jobs
     */
    @Override
    public List<Job> findInProgressJobs() {
        return  this.jobRepository.findByFilePathIsNotNull();
    }
}
