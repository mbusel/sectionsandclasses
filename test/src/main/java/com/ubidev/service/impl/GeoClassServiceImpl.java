package com.ubidev.service.impl;

import com.ubidev.entities.GeoClass;
import com.ubidev.repository.GeoClassRepository;
import com.ubidev.service.GeoClassService;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;

/**
 * GeoClass service
 */
@Service
@Transactional(Transactional.TxType.REQUIRED)
public class GeoClassServiceImpl extends BaseServiceImpl implements GeoClassService {

    private GeoClassRepository geoClassRepository;

    /**
     * GeoClass service constructor
     * @param geoClassRepository geo class repository
     */
    public GeoClassServiceImpl(GeoClassRepository geoClassRepository) {
        this.geoClassRepository = geoClassRepository;
    }

    /**
     * Find geo classes by name or/and code
     * @param name name
     * @param code code
     * @return geo classes
     */
    @Override
    public List<GeoClass> findByNameAndCode(String name, String code) {
        List<GeoClass> geoClasses;
        if (name != null && !name.isEmpty() && code != null && !code.isEmpty()) {
            geoClasses = this.geoClassRepository.findByNameContainingAndCodeContaining(name, code);
        } else if(name != null && !name.isEmpty()) {
            geoClasses = this.geoClassRepository.findByNameContaining(name);
        } else if(code != null && !code.isEmpty()) {
            geoClasses = this.geoClassRepository.findByCodeContaining(code);
        } else {
            geoClasses = this.geoClassRepository.findAll();
        }

        return geoClasses;
    }
}