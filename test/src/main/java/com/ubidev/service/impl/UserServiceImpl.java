package com.ubidev.service.impl;

import com.ubidev.repository.UserRepository;
import com.ubidev.entities.User;
import com.ubidev.service.UserService;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;

/**
 * User service
 */
@Service
@Transactional(Transactional.TxType.REQUIRED)
public class UserServiceImpl  extends BaseServiceImpl implements UserService {

    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    /**
     * User service constructor
     * @param userRepository user repository
     * @param passwordEncoder password encoder
     */
    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    /**
     * Create user
     * @param user user
     * @return user
     */
    @Override
    public User create(User user) {
        user.setPassword(this.passwordEncoder.encode(user.getPassword()));
        return this.userRepository.save(user);
    }

    /**
     * Read user by ID
     * @param userId user ID
     * @return user
     */
    @Override
    public User read(Long userId) {
        return this.userRepository.getOne(userId);
    }

    /**
     * Read all users sorted by last name, first name
     * @return all users
     */
    @Override
    public List<User> readAll() {
        return this.userRepository.findAll(Sort.by(Arrays.asList(Sort.Order.asc("lastName"), Sort.Order.asc("firstName"))));
    }

    /**
     * Update user
     * @param user user
     */
    @Override
    public void update(User user) {
        String password = user.getPassword();
        if (password != null) {
            user.setPassword(this.passwordEncoder.encode(password));
        }
        this.userRepository.save(user);
    }

    /**
     * Delete user by ID
     * @param userId user ID
     */
    @Override
    public void delete(Long userId) {
        this.userRepository.deleteById(userId);
    }
}