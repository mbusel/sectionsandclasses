package com.ubidev.service.impl;

import com.ubidev.entities.Section;
import com.ubidev.repository.SectionRepository;
import com.ubidev.service.SectionService;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Section service
 */
@Service
@Transactional(Transactional.TxType.REQUIRED)
public class SectionServiceImpl extends BaseServiceImpl implements SectionService {

    private SectionRepository sectionRepository;

    /**
     * Section service constructor
     * @param sectionRepository section repository
     */
    public SectionServiceImpl(SectionRepository sectionRepository) {
        this.sectionRepository = sectionRepository;
    }

    /**
     * Create section
     * @param section section
     * @return section
     */
    @Override
    public Section create(Section section) {
        return this.sectionRepository.save(section);
    }

    /**
     * Read section by ID
     * @param sectionId section ID
     * @return section
     */
    @Override
    public Section read(Long sectionId) {
        return this.sectionRepository.getOne(sectionId);
    }

    /**
     * Find section by job ID
     * @param jobId job ID
     * @return section
     */
    @Override
    public List<Section> findByJobId(Long jobId) {
        return this.sectionRepository.findByJobId(jobId);
    }
}