package com.ubidev.service;

import com.ubidev.entities.Job;
import com.ubidev.exception.FileUploadException;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;

public interface JobService {

    Job create(MultipartFile file) throws FileUploadException;

    Job read(Long jobId);

    List<Job> readAll();

    void update(Job job);

    List<Job> findInProgressJobs();
}