package com.ubidev.service;

import com.ubidev.entities.User;
import java.util.List;

public interface UserService {

    User create(User user);

    User read(Long userId);

    List<User> readAll();

    void update(User user);

    void delete(Long userId);
}