package com.ubidev.service;

import com.ubidev.entities.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface AuthService extends UserDetailsService {

    User findUserWithRoles(String email);

}
