package com.ubidev.utils;

import com.ubidev.entities.GeoClass;
import com.ubidev.entities.Section;
import com.ubidev.exception.XlsDocumentException;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.poi.openxml4j.exceptions.NotOfficeXmlFileException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class represents XLS utils to parse/create XLS file
 */
@Component
public class XlsUtils {

    protected Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    private Pattern classNamePattern;
    private Pattern classCodePattern;

    public XlsUtils() {
        this.classNamePattern = Pattern.compile("^class ([\\d]+) name$");
        this.classCodePattern = Pattern.compile("^class ([\\d]+) code$");
    }

    /**
     * Parse XLS file to sections
     * @param file file
     * @return sections
     */
    public List<Section> parseSectionsFromFile(File file) throws XlsDocumentException {

        List<Section> sections = null;
        FileInputStream stream = null;

        try {

            stream = new FileInputStream(file);
            Workbook workbook = new XSSFWorkbook(stream);
            Sheet datatypeSheet = workbook.getSheetAt(0);

            // find section name column
            int sectionNameRowIndex = -1;
            int sectionNameColumnIndex = -1;
            for (Row row : datatypeSheet) {
                for (Cell cell : row) {
                    String value = getCellValue(cell);
                    if (value != null) {
                        value = value.toLowerCase();
                        if ("section name".equals(value)) {
                            sectionNameRowIndex = row.getRowNum();
                            sectionNameColumnIndex = cell.getColumnIndex();
                        }
                    }
                }
            }

            if (sectionNameColumnIndex == -1) {
                throw new XlsDocumentException("Section name column not found");
            }

            // find class name / code columns
            Row header = datatypeSheet.getRow(sectionNameRowIndex);
            int headerNumberOfCells = header.getPhysicalNumberOfCells();
            if (headerNumberOfCells >= 3) { // Section name, Class name, Class code - minimum cells needed to parse
                Map<Integer, GeologicalClassCell> geoClassCells = new TreeMap<>(); // all classes should be sorted by index defined in XLS document
                for (Cell cell : header) {
                    int cellIndex = cell.getColumnIndex();
                    String value = getCellValue(cell);
                    if (value != null) {
                        value = value.toLowerCase();
                        boolean isClassName = true;
                        Integer classIndex = getClassCellIndex(value, this.classNamePattern);
                        if (classIndex == null) {
                            classIndex = getClassCellIndex(value, this.classCodePattern);
                            isClassName = false;
                        }

                        if (classIndex != null) {
                            GeologicalClassCell geoClassCell = geoClassCells.get(classIndex);
                            if (geoClassCell == null) {
                                geoClassCell = new GeologicalClassCell();
                                geoClassCells.put(classIndex, geoClassCell);
                            }

                            if (isClassName) {
                                geoClassCell.nameIndex = cellIndex;
                            } else {
                                geoClassCell.codeIndex = cellIndex;
                            }
                        }
                    }
                }

                if (geoClassCells.isEmpty()) {
                    throw new XlsDocumentException("No Class name/code columns found");
                }

                // parse rest of document
                for (Row row : datatypeSheet) {
                    if (row.getRowNum() > sectionNameRowIndex) {
                        String sectionName = getCellValue(row.getCell(sectionNameColumnIndex));
                        if (sectionName != null) {
                            Section section = null;
                            List<GeoClass> geoClasses = null;
                            for (GeologicalClassCell geoClassCell : geoClassCells.values()) {
                                String className = getCellValue(row.getCell(geoClassCell.nameIndex));
                                String classCode = getCellValue(row.getCell(geoClassCell.codeIndex));
                                if (className != null && classCode != null) {
                                    if (section == null) {
                                        geoClasses = new ArrayList<>();
                                        section = new Section(sectionName, geoClasses);
                                    }
                                    geoClasses.add(new GeoClass(className, classCode, section));
                                }
                            }

                            if (section != null) {
                                if (sections == null) {
                                    sections = new ArrayList<>();
                                }
                                sections.add(section);
                            }
                        }
                    }
                }

                if (sections == null) {
                    throw new XlsDocumentException("No geological classes found");
                }

            } else {
                throw new XlsDocumentException("No correct document header found");
            }

        } catch (IOException | NotOfficeXmlFileException e) {
            throw new XlsDocumentException(e.getMessage());
        } finally {
            IOUtils.closeQuietly(stream);
        }

        return sections;
    }

    /**
     * Get cell value
     * @param cell cell
     * @return value
     */
    private String getCellValue(Cell cell) {

        String value = null;
        if (cell.getCellType() == CellType.STRING) {
            String cellValue = cell.getStringCellValue().trim();
            if (!cellValue.isEmpty()) {
                value = cellValue;
            }
        } else if (cell.getCellType() == CellType.NUMERIC) {
            value = Double.toString(cell.getNumericCellValue());
        }

        return value;
    }

    /**
     * Get class index from cell valu
     * @param value cell value
     * @param pattern regExp pattern
     * @return index
     */
    private Integer getClassCellIndex(String value, Pattern pattern) {

        Integer index = null;
        Matcher matcher = pattern.matcher(value);
        if (matcher.find()) {
            index = Integer.valueOf(matcher.group(1));
        }

        return index;
    }

    /**
     * Create XLS file from sections
     * @param sections sections
     * @return XLS file
     */
    public File createFileFromSections(List<Section> sections) throws XlsDocumentException {

        // Determine max classes
        int maxClassesCount = 0;
        for (Section section : sections) {
            int geoClassesCount = section.getGeoClasses().size();
            if (geoClassesCount > maxClassesCount) {
                maxClassesCount = geoClassesCount;
            }
        }

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Sections");

        int rowIndex = 0;
        int cellIndex = 0;

        Row row = sheet.createRow(rowIndex++); // create header row
        row.createCell(cellIndex++).setCellValue("Section name");
        for (int i = 1; i <= maxClassesCount; i++) {
            row.createCell(cellIndex++).setCellValue(String.format("Class %d name", i));
            row.createCell(cellIndex++).setCellValue(String.format("Class %d code", i));
        }

        for (Section section : sections) {
            cellIndex = 0;
            row = sheet.createRow(rowIndex++);
            row.createCell(cellIndex++).setCellValue(section.getName());
            List<GeoClass> geoClasses = section.getGeoClasses();
            for (GeoClass geologicalClass : geoClasses) {
                row.createCell(cellIndex++).setCellValue(geologicalClass.getName());
                row.createCell(cellIndex++).setCellValue(geologicalClass.getCode());
            }
        }

        maxClassesCount = maxClassesCount * 2;
        for (int i = 0; i <= maxClassesCount; i++) {
            sheet.autoSizeColumn(i);
        }

        File tempFile;
        FileOutputStream outputStream = null;
        try {
            Path tempFilePath = Files.createTempFile("doc", ".xls");
            tempFile = new File(tempFilePath.toAbsolutePath().toString());
            outputStream = new FileOutputStream(tempFile);

            workbook.write(outputStream);
            workbook.close();
        } catch (IOException e) {
            LOGGER.error("Unable to create XLS document", e);
            throw new XlsDocumentException("Unable to create XLS document");
        } finally {
            IOUtils.closeQuietly(outputStream);
        }

        return tempFile;
    }

    /**
     * This class is dedicated to keep found class name/code column indexes
     */
    private static class GeologicalClassCell {
        int nameIndex;
        int codeIndex;
    }
}