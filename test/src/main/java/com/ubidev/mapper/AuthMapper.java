package com.ubidev.mapper;

import com.ubidev.dto.AuthDto;
import com.ubidev.entities.User;
import com.ubidev.entities.UserRole;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Auth to/from AuthDto mapper
 */
@Mapper(componentModel = "spring")
public interface AuthMapper {

    AuthMapper INSTANCE = Mappers.getMapper(AuthMapper.class);

    @Mapping(target = "roles", ignore = true)
    AuthDto fromUser(User user);

    default AuthDto fromUserWithRoles(User user) {
        AuthDto authDto = INSTANCE.fromUser(user);
        Collection<UserRole> roles = user.getRoles();
        if (roles != null && !roles.isEmpty()) {
            authDto.setRoles(roles.stream()
                    .map(UserRole::getName)
                    .collect(Collectors.toList()));
        }
        return authDto;
    }
}
