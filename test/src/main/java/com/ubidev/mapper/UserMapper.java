package com.ubidev.mapper;

import com.ubidev.dto.UserDto;
import com.ubidev.entities.User;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import java.util.List;

/**
 * User to/from UserDto mapper
 */
@Mapper(componentModel = "spring")
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    User toUser(UserDto userDto);
    UserDto fromUser(User user);

    List<User> toUsers(List<UserDto> usersDto);
    List<UserDto> fromUsers(List<User> users);
}
