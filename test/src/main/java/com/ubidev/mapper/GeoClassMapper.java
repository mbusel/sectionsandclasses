package com.ubidev.mapper;

import com.ubidev.dto.GeoClassDto;
import com.ubidev.entities.GeoClass;
import com.ubidev.entities.Section;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import java.util.ArrayList;
import java.util.List;

/**
 * GeoClass to/from GeoClassDto mapper
 */
@Mapper(componentModel = "spring")
public interface GeoClassMapper {

    GeoClassMapper INSTANCE = Mappers.getMapper(GeoClassMapper.class);

    @Mapping(target = "section", ignore = true)
    GeoClassDto fromGeoClass(GeoClass geoClass);

    default GeoClassDto fromGeoClassWithSection(GeoClass geoClass) {
        GeoClassDto geoClassDto = INSTANCE.fromGeoClass(geoClass);

        Section section = geoClass.getSection();
        if (section != null) {
            geoClassDto.setSection(section.getName());
        }

        return geoClassDto;
    }

    default List<GeoClassDto> fromGeoClassesWithSection(List<GeoClass> geoClasses) {
        List<GeoClassDto> geoClassDtos = new ArrayList<>();
        for (GeoClass geoClass: geoClasses) {
            geoClassDtos.add(fromGeoClassWithSection(geoClass));
        }

        return geoClassDtos;
    }
}
