package com.ubidev.mapper;

import com.ubidev.dto.JobDto;
import com.ubidev.entities.Job;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import java.util.ArrayList;
import java.util.List;

/**
 * Job to/from JobDto mapper
 */
@Mapper()
public interface JobMapper {

    JobMapper INSTANCE = Mappers.getMapper(JobMapper.class);

    JobDto fromJob(Job job);

    default JobDto fromJobWithReadyStatus(Job job) {
        JobDto jobDto = INSTANCE.fromJob(job);
        String filePath = job.getFilePath();
        jobDto.setReady(filePath == null || filePath.isEmpty());
        return jobDto;
    }

    default List<JobDto> fromJobsWithReadyStatus(List<Job> jobs) {
        List<JobDto> jobDtos = new ArrayList<>();
        for (Job job: jobs) {
            jobDtos.add(fromJobWithReadyStatus(job));
        }

        return jobDtos;
    }
}
