package com.ubidev.security.jwt;

import com.ubidev.service.AuthService;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

// Don't use any @Component/@Configuration annotation here, because in this case this filter will be
// used for every request automatically, but it should work only for REST. Please check SecurityConfigurerAdapter.
public class JwtRequestFilter extends OncePerRequestFilter {

    private AuthService userDetailsService;
    private JwtTokenUtil tokenUtil;

    public JwtRequestFilter(AuthService userDetailsService, JwtTokenUtil tokenUtil) {
        this.userDetailsService = userDetailsService;
        this.tokenUtil = tokenUtil;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {

        String requestTokenHeader = request.getHeader("Authorization");

        String username = null;
        String jwtToken = null;

        this.logger.debug("JWT request filter checks token");

        // JWT Token is in the form "Bearer token". Remove Bearer word and get only the Token
        if (requestTokenHeader != null) {
            if (requestTokenHeader.startsWith("Bearer ")) {
                jwtToken = requestTokenHeader.substring(7);
                if (jwtToken.isEmpty()) {
                    this.logger.warn("JWT token is empty");
                } else {
                    try {
                        username = this.tokenUtil.getUsernameFromToken(jwtToken);
                    } catch (IllegalArgumentException e) {
                        this.logger.warn("Unable to get JWT Token");
                    } catch (ExpiredJwtException e) {
                        this.logger.warn("JWT token has expired");
                    } catch (SignatureException e) {
                        this.logger.warn("JWT signature is not valid");
                    } catch (MalformedJwtException e) {
                        this.logger.warn("JWT token is not valid");
                    }
                }
            } else {
                this.logger.warn("JWT Token does not begin with Bearer String");
            }
        }

        // Once we get the token validate it.
        if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {

            UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);
            if (userDetails.isEnabled()) {
                // if token is valid configure Spring Security to manually set authentication
                if (this.tokenUtil.validateToken(jwtToken, userDetails)) {

                    String token = this.tokenUtil.generateToken(userDetails);

                    // Set new token to request header (always send new token in response header)
                    response.setHeader("Access-Control-Expose-Headers", JwtTokenUtil.RESPONSE_HEADER_NAME);
                    response.setHeader(JwtTokenUtil.RESPONSE_HEADER_NAME, token);

                    UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken
                            = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                    usernamePasswordAuthenticationToken
                            .setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                    // After setting the Authentication in the context, we specify that the current user is authenticated.
                    // So it passes the Spring Security Configurations successfully.
                    SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                }
            } else {
                this.logger.warn("User disabled");
            }
        }
        chain.doFilter(request, response);
    }
}
