package com.ubidev.security;

import com.ubidev.entities.User;
import com.ubidev.entities.UserRole;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Security utils class
 */
@Component
public class SecurityUtil {

    /**
     * Convert user to user detail
     *
     * @param user user
     *
     * @return user details
     */
    public UserDetails toUserDetails(User user) {
        Collection<UserRole> roles = user.getRoles();
        Collection<? extends GrantedAuthority> authorities = mapRolesToAuthorities(roles);
        return new org.springframework.security.core.userdetails.User(user.getEmail(),
                user.getPassword(),  user.getActive(), true, true, true, authorities);
    }

    /**
     * Map roles to authorities
     *
     * @param roles roles
     *
     * @return authorities
     */
    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<UserRole> roles){
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority("ROLE_" + role.getName()))
                .collect(Collectors.toList());
    }
}
