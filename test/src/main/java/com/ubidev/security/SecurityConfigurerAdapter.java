package com.ubidev.security;

import com.ubidev.security.jwt.JwtRequestFilter;
import com.ubidev.security.jwt.JwtTokenUtil;
import com.ubidev.service.AuthService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

public class SecurityConfigurerAdapter {

    @Configuration
    @Order(1)
    public static class ApiSecurityConfigAdapter extends WebSecurityConfigurerAdapter {

        private AuthenticationProvider authenticationProvider;
        private JwtRequestFilter requestFilter;

        public ApiSecurityConfigAdapter(AuthenticationProvider authenticationProvider,
                                        AuthService authService, JwtTokenUtil tokenUtil) {
            this.authenticationProvider = authenticationProvider;
            this.requestFilter = new JwtRequestFilter(authService, tokenUtil);
        }

        @Override
        protected void configure(AuthenticationManagerBuilder auth) {
            auth.authenticationProvider(this.authenticationProvider);
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {

            http
                    .csrf().disable() // disable CSRF
                    .antMatcher("/api/**")
                    .addFilterBefore(this.requestFilter, UsernamePasswordAuthenticationFilter.class)
                    .authorizeRequests().anyRequest().permitAll()
                    .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        }

        @Bean
        @Override
        public AuthenticationManager authenticationManagerBean() throws Exception {
            return super.authenticationManagerBean();
        }
    }
}