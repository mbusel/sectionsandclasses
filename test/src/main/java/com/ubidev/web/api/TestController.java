package com.ubidev.web.api;

import com.ubidev.entities.User;
import com.ubidev.entities.UserRole;
import com.ubidev.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import java.util.Arrays;

/**
 * This is test REST service just to create some test users quickly
 */
@RestController
@RequestMapping("/api")
public class TestController extends BaseController {

    private UserService userService;

    public TestController(UserService userService) {
        this.userService = userService;
    }

    // http://localhost:8080/api/create_test_users
    @RequestMapping(value = "/create_test_users", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public String createTestUsers() {
        User admin = new User();
        admin.setFirstName("Maxim");
        admin.setLastName("Busel");
        admin.setEmail("max@ubidev.com");
        admin.setPassword("123456");
        admin.setActive(true);

        UserRole adminRole = new UserRole(UserRole.ROLE_ADMIN);
        adminRole.setUser(admin);
        admin.setRoles(Arrays.asList(adminRole));
        this.userService.create(admin);

        User user = new User();
        user.setFirstName("Helen");
        user.setLastName("Busel");
        user.setEmail("helen@ubidev.com");
        user.setPassword("654321");
        user.setActive(true);

        UserRole userRole = new UserRole(UserRole.ROLE_USER);
        userRole.setUser(user);
        user.setRoles(Arrays.asList(userRole));
        this.userService.create(user);

        return "Two users created!";
    }
}
