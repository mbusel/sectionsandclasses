package com.ubidev.web.api;

import com.ubidev.entities.User;
import com.ubidev.exception.Error;
import com.ubidev.exception.AuthException;
import com.ubidev.mapper.AuthMapper;
import com.ubidev.security.SecurityUtil;
import com.ubidev.security.jwt.JwtTokenUtil;
import com.ubidev.service.AuthService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import java.util.Map;

/**
 * Auth REST controller
 */
@CrossOrigin
@RestController
@RequestMapping("/api/auth")
public class AuthController extends BaseController {

    private AuthenticationManager authManager;
    private AuthService authService;
    private AuthMapper authMapper;
    private JwtTokenUtil tokenUtil;
    private SecurityUtil securityUtil;

    /**
     * Auth REST controller constructor
     * @param authenticationManager auth manager
     * @param authService auth service
     * @param tokenUtil token utils
     * @param securityUtil security utils
     */
    public AuthController(AuthenticationManager authenticationManager,
                          AuthService authService, JwtTokenUtil tokenUtil, SecurityUtil securityUtil) {
        this.authManager = authenticationManager;
        this.authService = authService;
        this.authMapper = AuthMapper.INSTANCE;
        this.tokenUtil = tokenUtil;
        this.securityUtil = securityUtil;
    }

    /**
     * Create auth token
     * @param body JSON object with user name and password parameters
     * @return response
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> createAuthToken(@RequestBody Map<String, String> body) throws AuthException {

        String username = body.get("u");
        String password = body.get("p");

        if (username == null || password == null) {
            throw new AuthException(Error.ERROR_INVALID_CREDENTIALS, "Invalid credentials");
        }

        authenticate(username, password);

        User user = this.authService.findUserWithRoles(username);
        if (user == null) {
            throw new AuthException(Error.ERROR_INVALID_CREDENTIALS, "Invalid credentials");
        }

        UserDetails userDetails = this.securityUtil.toUserDetails(user);

        String token = this.tokenUtil.generateToken(userDetails);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Access-Control-Expose-Headers", JwtTokenUtil.RESPONSE_HEADER_NAME);
        responseHeaders.set(JwtTokenUtil.RESPONSE_HEADER_NAME, token);

        return ResponseEntity.ok()
                .headers(responseHeaders)
                .body(this.authMapper.fromUserWithRoles(user));
    }

    /**
     * Authenticate user
     * @param username user name
     * @param password password
     */
    private void authenticate(String username, String password) throws AuthException {
        try {
            this.authManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new AuthException(Error.ERROR_USER_DISABLED, "User disabled");
        } catch (BadCredentialsException e) {
            throw new AuthException(Error.ERROR_INVALID_CREDENTIALS, "Invalid credentials");
        }
    }
}
