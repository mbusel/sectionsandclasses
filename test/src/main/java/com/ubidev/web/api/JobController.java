package com.ubidev.web.api;

import com.ubidev.dto.JobDto;
import com.ubidev.entities.Job;
import com.ubidev.entities.Section;
import com.ubidev.exception.FileUploadException;
import com.ubidev.exception.XlsDocumentException;
import com.ubidev.mapper.JobMapper;
import com.ubidev.service.JobService;
import com.ubidev.service.SectionService;
import com.ubidev.utils.XlsUtils;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Job REST controller
 */
@CrossOrigin
@RestController
@RequestMapping("/api/job")
public class JobController extends BaseController {

    private JobService jobService;
    private SectionService sectionService;
    private JobMapper jobMapper;
    private XlsUtils xlsUtils;

    /**
     * Job REST controller constructor
     * @param jobService job service
     * @param sectionService section service
     * @param xlsUtils XLS utils
     */
    public JobController(JobService jobService, SectionService sectionService, XlsUtils xlsUtils) {
        this.jobService = jobService;
        this.sectionService = sectionService;
        this.xlsUtils = xlsUtils;
        this.jobMapper = JobMapper.INSTANCE;
    }

    /**
     * Upload XLS file (create job)
     * @param file file
     * @return response
     */
    @PostMapping(value = "/upload")
    @ResponseStatus(HttpStatus.CREATED)
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public Long upload(@RequestParam("file") MultipartFile file) throws FileUploadException {
        Job job = this.jobService.create(file);
        return job.getId();
    }

    /**
     * Download XLS file (sections and geo classes)
     * @param jobId job ID
     * @return response
     */
    @GetMapping(value = "/download/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<ByteArrayResource> download(@PathVariable("id") Long jobId) throws XlsDocumentException, IOException {

        List<Section> sections = this.sectionService.findByJobId(jobId);

        String fileName = "Document_" + jobId + ".xlsx";
        HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);
        header.add("Cache-Control", "no-cache, no-store, must-revalidate");
        header.add("Pragma", "no-cache");
        header.add("Expires", "0");

        File file = this.xlsUtils.createFileFromSections(sections);
        Path path = Paths.get(file.getAbsolutePath());
        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));
        return ResponseEntity.ok()
                .headers(header)
                .contentLength(file.length())
                .contentType(MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
                .body(resource);
    }

    /**
     * Get all jobs
     * @return response
     */
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public List<JobDto> readAll()  {
        List<Job> users = this.jobService.readAll();
        return this.jobMapper.fromJobsWithReadyStatus(users);
    }
}