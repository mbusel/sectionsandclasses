package com.ubidev.web.api;

import com.ubidev.dto.GeoClassDto;
import com.ubidev.entities.GeoClass;
import com.ubidev.mapper.GeoClassMapper;
import com.ubidev.service.GeoClassService;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * GeoClass REST controller
 */
@CrossOrigin
@RestController
@RequestMapping("/api/class")
public class GeoClassController extends BaseController {

    private GeoClassService geoClassService;
    private GeoClassMapper geoClassMapper;

    /**
     * GeoClass REST controller constructor
     * @param geoClassService geo class service
     */
    public GeoClassController(GeoClassService geoClassService) {
        this.geoClassService = geoClassService;
        this.geoClassMapper = GeoClassMapper.INSTANCE;
    }

    /**
     * Search geo classes by name or/and code
     * @param name name
     * @param code code
     * @return response
     */
    @GetMapping(value = "/search")
    @ResponseStatus(HttpStatus.OK)
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public List<GeoClassDto> search(@RequestParam(value = "name") String name, @RequestParam(value = "code") String code)  {
        List<GeoClass> geoClasses = this.geoClassService.findByNameAndCode(name, code);
        return this.geoClassMapper.fromGeoClassesWithSection(geoClasses);
    }
}
