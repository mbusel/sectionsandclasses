package com.ubidev.web.api;

import com.ubidev.dto.UserDto;
import com.ubidev.entities.User;
import com.ubidev.mapper.UserMapper;
import com.ubidev.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * User REST controller
 */
@CrossOrigin
@RestController
@RequestMapping("/api/user")
public class UserController extends BaseController {

    private UserService userService;
    private UserMapper userMapper;

    /**
     * User REST controller constructor
     * @param userService user service
     */
    public UserController(UserService userService) {
        this.userService = userService;
        this.userMapper = UserMapper.INSTANCE;
    }

    /**
     * Create user
     * @param userDto user
     * @return response
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Secured({"ROLE_ADMIN"})
    public Long create(@RequestBody UserDto userDto) {
        User user = this.userMapper.toUser(userDto);
        user = this.userService.create(user);
        return user.getId();
    }

    /**
     * Read user by ID
     * @param id user ID
     * @return response
     */
    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Secured({"ROLE_ADMIN"})
    public UserDto read(@PathVariable("id") Long id) {
        User user = this.userService.read(id);
        return this.userMapper.fromUser(user);
    }

    /**
     * Read all users
     * @return response
     */
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @Secured({"ROLE_ADMIN"})
    public List<UserDto> readAll()  {
        List<User> users = this.userService.readAll();
        return this.userMapper.fromUsers(users);
    }

    /**
     * Update user
     * @param id user ID
     * @param userDto user
     */
    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Secured({"ROLE_ADMIN"})
    public void update(@PathVariable( "id" ) Long id, @RequestBody UserDto userDto) {
        User user = this.userMapper.toUser(userDto);
        user.setId(id);
        this.userService.update(user);
    }

    /**
     * Delete user by ID
     * @param id user ID
     */
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Secured({"ROLE_ADMIN"})
    public void delete(@PathVariable("id") Long id) {
        this.userService.delete(id);
    }
}