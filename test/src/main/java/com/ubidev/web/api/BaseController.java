package com.ubidev.web.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.PostConstruct;

/**
 * Base class for all REST controllers
 */
public abstract class BaseController {

    protected Logger LOGGER;

    @PostConstruct
    public void initializeLogger() {
        LOGGER = LoggerFactory.getLogger(this.getClass());
    }
}