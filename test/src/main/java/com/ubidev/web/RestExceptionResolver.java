package com.ubidev.web;

import com.ubidev.dto.ErrorDto;
import com.ubidev.exception.AuthException;
import com.ubidev.exception.BaseException;
import com.ubidev.exception.Error;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import javax.servlet.http.HttpServletRequest;

/**
 * This class allows to dedicate individual method to each exception
 */
public class RestExceptionResolver {

    @RestControllerAdvice
    @Order(1)
    public static class AuthenticationExceptionResolver {

        @ExceptionHandler({AuthenticationException.class})
        @ResponseStatus(HttpStatus.UNAUTHORIZED)
        public ErrorDto handleException(HttpServletRequest request, Exception e) {
            return new ErrorDto(Error.ERROR_UNAUTHORIZED, e.getMessage());
        }
    }

    @RestControllerAdvice
    @Order(2)
    public static class AccessDeniedExceptionResolver {

        @ExceptionHandler({AccessDeniedException.class})
        @ResponseStatus(HttpStatus.FORBIDDEN)
        public ErrorDto handleException(HttpServletRequest request, Exception e) {
            return new ErrorDto(Error.ERROR_ACCESS_DENIED, e.getMessage());
        }
    }

    @RestControllerAdvice
    @Order(3)
    public static class AuthExceptionResolver {

        @ExceptionHandler({AuthException.class})
        @ResponseStatus(HttpStatus.UNAUTHORIZED)
        public ErrorDto handleException(HttpServletRequest request, Exception e) {
            AuthException authException = (AuthException) e;
            return new ErrorDto(authException.getCode(), authException.getMessage());
        }
    }

    @RestControllerAdvice
    @Order(4)
    public static class BaseExceptionResolver {

        @ExceptionHandler({BaseException.class})
        @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
        public ErrorDto handleException(HttpServletRequest request, Exception e) {
            BaseException baseException = (BaseException) e;
            return new ErrorDto(baseException.getCode(), baseException.getMessage());
        }
    }

    @RestControllerAdvice
    @Order(5)
    public static class ExceptionResolver {

        @ExceptionHandler({Exception.class})
        @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
        public ErrorDto handleException(HttpServletRequest request, Exception e) {
            return new ErrorDto("500", e.getMessage());
        }
    }
}