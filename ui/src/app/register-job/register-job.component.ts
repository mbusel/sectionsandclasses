import {Component, OnInit, ViewChild} from '@angular/core';
import {JobService} from '../service/job.service';
import {NavigationService} from '../service/navigation.service';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {Config} from '../configuration';

export interface Job {
  id: number;
  ready: string;
  error: string;
}

@Component({
  selector: 'app-register-job',
  templateUrl: './register-job.component.html',
  styleUrls: ['./register-job.component.css']
})
export class RegisterJobComponent implements OnInit {

  displayedColumns: string[] = ['id', 'ready', 'error', 'download'];
  downloadUrl;
  jobs;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private jobService: JobService,
    private navigationService: NavigationService
  ) { }

  ngOnInit(): void {
    this.downloadUrl = Config.BASE_API_URL + 'job/download/';
    this.navigationService.showTaskBackendMenu();
    this.loadJobs();
  }

  loadJobs() {
    this.jobService.readJobs().subscribe(
      (data: Job[]) => {
        if (data) {
          this.jobs = new MatTableDataSource<Job>(data);
          this.jobs.sort = this.sort;
          this.jobs.paginator = this.paginator;
        }
      }
    );
  }

  onClickToUpload() {
    const fileUpload = document.getElementById('fileUpload') as HTMLInputElement;
    fileUpload.onchange = () => {
      if (fileUpload.files[0]) {
        this.jobService.uploadXLSFile(fileUpload.files[0]).subscribe(
          data => {
            if (data) {
              this.loadJobs();
            }
          }
        );
      }
    };
    fileUpload.click();
  }
}
