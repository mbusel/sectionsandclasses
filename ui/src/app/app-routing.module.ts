import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {RegisterJobComponent} from './register-job/register-job.component';
import {SearchClassesComponent} from './search-classes/search-classes.component';

const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'register-job', component: RegisterJobComponent},
  {path: 'search-classes', component: SearchClassesComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
