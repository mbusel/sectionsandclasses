import {Component, OnInit} from '@angular/core';
import {AuthService} from './service/auth.service';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';
import {NavigationService} from './service/navigation.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  isLoggedIn$: Observable<boolean>;
  isMenuButtonVisible$: Observable<boolean>;
  isTaskBackendMenuVisible$: Observable<boolean>;

  constructor(
    private router: Router,
    private authService: AuthService,
    private navigationService: NavigationService
  ) { }

  ngOnInit(): void {
    this.isMenuButtonVisible$ = this.navigationService.isMenuButtonVisible;
    this.isTaskBackendMenuVisible$ = this.navigationService.isTaskBackendMenuVisible;
    this.isLoggedIn$ = this.authService.isLoggedIn;
    this.authService.resetLoggedIn();
  }

  navigateToRegisterJob() {
    this.router.navigate(['register-job']);
  }

  navigateToSearchGeoClasses() {
    this.router.navigate(['search-classes']);
  }

  logout() {
    this.authService.clearToken();
    this.router.navigate(['']);
  }
}
