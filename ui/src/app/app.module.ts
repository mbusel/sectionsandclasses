import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import {AppRoutingModule} from './app-routing.module';
import {MaterialModule} from './material.module';
import {AppHttpInterceptor} from './app-http.interceptor';

import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import { ErrorComponent } from './error/error.component';
import { ChartComponent } from './chart/chart.component';
import { RegisterJobComponent } from './register-job/register-job.component';
import { SearchClassesComponent } from './search-classes/search-classes.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ErrorComponent,
    ChartComponent,
    RegisterJobComponent,
    SearchClassesComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MaterialModule,
    AppRoutingModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AppHttpInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
