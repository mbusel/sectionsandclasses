import {Input, Component, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Input() error: string | null;

  form: FormGroup;
  auth;

  constructor(
    private router: Router,
    private authService: AuthService
  ) {}

  submit() {
    if (this.form.valid) {
      const value = this.form.value;
      this.authService.login(value.username, value.password).subscribe(
        data => {
          if (data) {
            this.router.navigate(['register-job']);
          }
        }
      );
    }
  }

  ngOnInit(): void {
    this.authService.clearToken();
    this.form = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.email, Validators.maxLength(254)]),
      password: new FormControl('', [Validators.required, Validators.maxLength(10)]),
    });
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.form.controls[controlName].hasError(errorName);
  }
}
