import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Config} from '../configuration';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient) {}

  login(username, password) {
    const url = Config.BASE_API_URL + 'auth';
    return this.http.post(url, {
      u: username,
      p: password
    });
  }

  setToken(token) {
    localStorage.setItem('token', token);
    this.resetLoggedIn();
  }

  getToken() {
    return localStorage.getItem('token');
  }

  clearToken() {
    localStorage.setItem('token', '');
    this.resetLoggedIn();
  }

  resetLoggedIn() {
    setTimeout(() => {
      this.loggedIn.next(this.getToken() !== '');
    });
  }

  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }
}
