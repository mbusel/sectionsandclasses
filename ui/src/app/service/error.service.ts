import {Injectable} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {ErrorComponent} from '../error/error.component';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  constructor(private dialog: MatDialog) {}

  openDialog(errorData) {
    const dialogRef = this.dialog.open(ErrorComponent, {
      width: '300px',
      data: errorData
    });
  }
}
