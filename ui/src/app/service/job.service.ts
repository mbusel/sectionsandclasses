import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Config} from '../configuration';

@Injectable({
  providedIn: 'root'
})
export class JobService {

  constructor(
    private http: HttpClient
  ) { }

  uploadXLSFile(fileToUpload: File) {
    const formData = new FormData();
    formData.set('file', fileToUpload, fileToUpload.name);

    const url = Config.BASE_API_URL + 'job/upload';
    return this.http.post(url, formData, {
      headers: {}
    });
  }

  readJobs() {
    const url = Config.BASE_API_URL + 'job';
    return this.http.get(url);
  }
}
