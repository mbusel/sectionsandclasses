import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  private menuButtonVisible: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private taskBackendMenuVisible: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor() { }

  get isMenuButtonVisible() {
    return this.menuButtonVisible.asObservable();
  }

  get isTaskBackendMenuVisible() {
    return this.taskBackendMenuVisible.asObservable();
  }

  showTaskBackendMenu() {
    this.hideMenus();
    this.setTaskBackendMenuVisibility(true);
    this.setMenuButtonVisibility(true);

  }

  // hide any visible menu
  hideMenus() {
    this.setMenuButtonVisibility(false);
    this.setTaskBackendMenuVisibility(false);
  }

  setMenuButtonVisibility(visible) {
    setTimeout(() => {
      this.menuButtonVisible.next(visible);
    });
  }

  setTaskBackendMenuVisibility(visible) {
    setTimeout(() => {
      this.taskBackendMenuVisible.next(visible);
    });
  }
}
