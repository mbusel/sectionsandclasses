import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Config} from '../configuration';

@Injectable({
  providedIn: 'root'
})
export class GeoClassService {

  constructor(
    private http: HttpClient
  ) { }

  searchGeoClasses(name, code) {
    const url = Config.BASE_API_URL + 'class/search';
    return this.http.get(url, {
      params: {name, code}
    });
  }
}
