import {Injectable} from '@angular/core';
import {Observable, of, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {Router} from '@angular/router';
import {AuthService} from './service/auth.service';
import {ErrorService} from './service/error.service';

import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpErrorResponse, HttpResponse
} from '@angular/common/http';

@Injectable()
export class AppHttpInterceptor implements HttpInterceptor {

  constructor(
    private router: Router,
    private authService: AuthService,
    private errorService: ErrorService
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const isAuthRequest = request.url.endsWith('/auth');
    const token = this.authService.getToken();

    if (!isAuthRequest) {
      if (token) {
        request = this.addAuthenticationToken(token, request);
      } else {
        this.router.navigate(['']);
        return of(new HttpResponse( // cancel request
          { status: 200, body: '' }
        ));
      }
    }

    const isUploadRequest = request.method === 'POST' && request.url.endsWith('/job/upload');
    if (!isUploadRequest && !request.headers.has('Content-Type')) {
      request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
    }

    request = request.clone({ headers: request.headers.set('Accept', 'application/json') });

    return next.handle(request).pipe(
      map(event => {
        if (event instanceof HttpResponse) {
          event = event.clone({body: this.handleResponse(event.body)});
          this.authService.setToken(event.headers.get('Authorization'));
        }
        return event;
      }),
      catchError(this.handleError)
    );
  }

  private addAuthenticationToken = (token: string, request: HttpRequest<any>): HttpRequest<any> => {
    // If you are calling an outside domain then do not add the token.
    if (request.url.match(/www.somecurrentdomain.com\//)) {
      return request;
    }
    return request.clone({
      headers: request.headers.set('Authorization', 'Bearer ' + token)
    });
  }

  // If response body contains error then return null value instead error JSON object
  // If response body doesn't contain error pass JSON object to component
  private handleResponse = (body: HttpResponse<any>) => {
    // @ts-ignore
    return body.error ? null : body;
  }

  // If response has any auth errors then navigate to login page (logout)
  // If response has any non auth error then show error dialog.
  private handleError = (errorResponse: HttpErrorResponse) => {

    const status = errorResponse.status;

    const body = errorResponse.error;
    const errorCode = body.error ? body.error : '000';
    const errorMessage = body.message ? body.message : errorResponse.message;

    if ((status === 401 || status === 403) && errorCode !== '102' && errorCode !== '103') {
      // HTTP status UNAUTHORIZED or FORBIDDEN
      // 102 - invalid credentials, 103 - user is disabled
      this.authService.clearToken();
      this.router.navigate(['']);
    } else {
      this.errorService.openDialog({
        // @ts-ignore
        message: errorMessage,
        code: errorCode
      });
    }

    // return an observable with a user-facing error message
    return throwError(errorMessage);
  }
}
