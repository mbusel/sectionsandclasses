import {Component, OnInit, ViewChild} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {GeoClassService} from '../service/geo-class.service';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {NavigationService} from '../service/navigation.service';

export interface GeoClass {
  section: string;
  name: string;
  code: string;
}

@Component({
  selector: 'app-search-classes',
  templateUrl: './search-classes.component.html',
  styleUrls: ['./search-classes.component.css']
})
export class SearchClassesComponent implements OnInit {

  form: FormGroup;

  displayedColumns: string[] = ['section', 'name', 'code'];
  geoClasses;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private geoClassService: GeoClassService,
    private navigationService: NavigationService
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      className: new FormControl(''),
      classCode: new FormControl(''),
    });
    this.navigationService.showTaskBackendMenu();
    this.searchGeoClasses('', '');
  }

  searchGeoClasses(name, code) {
    this.geoClassService.searchGeoClasses(name, code).subscribe(
      (data: GeoClass[]) => {
        if (data) {
          this.geoClasses = new MatTableDataSource<GeoClass>(data);
          this.geoClasses.sort = this.sort;
          this.geoClasses.paginator = this.paginator;
        }
      }
    );
  }

  submit() {
    const value = this.form.value;
    this.searchGeoClasses(value.className, value.classCode);
  }
}
